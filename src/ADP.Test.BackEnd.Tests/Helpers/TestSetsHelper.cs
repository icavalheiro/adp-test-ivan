using System;
using System.Collections.Generic;
using ADP.Test.BackEnd.Models;

namespace ADP.Test.BackEnd.Tests.Helpers
{
    public static class TestSetsHelper
    {
        public static List<Question> GetQuestionsTestSet()
        {
            var questions = new List<Question>
            {
                new Question
                {
                    Id = Guid.Parse("55f849d3-db94-4a05-af4f-4bb58f52922c"),
                    Category = "Entertainment: Comedy",
                    Difficulty = "hard",
                    Text = "Lorem Ipsum",
                    Options = new string []
                    {
                        "True",
                        "False"
                    },
                    RightOption = 0
                },
                new Question
                {
                    Id = Guid.Parse("9a4271ee-df30-465f-9d57-1a2a8a0d2d1e"),
                    Category = "Entertainment: Music",
                    Difficulty = "hard",
                    Text = "Dolor sit amet",
                    Options = new string []
                    {
                        "True",
                        "False"
                    },
                    RightOption = 1
                }
            };

            return questions;
        }
    }
}