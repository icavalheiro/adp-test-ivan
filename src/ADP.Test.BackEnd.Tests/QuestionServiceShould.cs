using System;
using System.Linq;
using System.Threading.Tasks;
using ADP.Test.BackEnd.Data;
using ADP.Test.BackEnd.Services;
using ADP.Test.BackEnd.Tests.Helpers;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;
using Xunit;

namespace ADP.Test.BackEnd.Tests
{
    public class QuestionServiceShould
    {
        private static DatabaseContext Database => GetDb();
        private static ILogger<QuestionService> Logger => new Mock<ILogger<QuestionService>>().Object;
        private static DatabaseContext GetDb()
        {
            var databaseMock = new Mock<DatabaseContext>();

            var questions = TestSetsHelper.GetQuestionsTestSet();

            databaseMock
                .Setup(db => db.Questions)
                .ReturnsDbSet(questions);
            
            return databaseMock.Object;
        }
        

        [Fact]
        public async Task ReturnNullIfNotFound()
        {
            var instance = new QuestionService(Database, Logger);
            var invalidGuid = Guid.Parse("55555555-dddd-aaaa-bbbb-cccccccccccc");

            var observed = await instance.GetQuestionAsync(invalidGuid);
            
            Assert.Null(observed);
        }
        
        
        [Fact]
        public async Task ReturnAQuestion()
        {
            var instance = new QuestionService(Database, Logger);

            var firstItemInSet = TestSetsHelper.GetQuestionsTestSet()[0];

            var observed = await instance.GetQuestionAsync(firstItemInSet.Id);
            
            Assert.NotNull(observed);
        }
        
        [Fact]
        public async Task ReturnTheRightQuestion()
        {
            var instance = new QuestionService(Database, Logger);
            
            var firstItemInSet = TestSetsHelper.GetQuestionsTestSet()[0];
            
            var observed = (await instance.GetQuestionAsync(firstItemInSet.Id)).Text;
            var expected = firstItemInSet.Text;
            
            Assert.Equal(expected, observed);
        }

        [Fact]
        public async Task ReturnAllQuestions()
        {
            var instance = new QuestionService(Database, Logger);

            var observed = (await instance.GetAllQuestionsAsync()).Count();
            var expected = TestSetsHelper.GetQuestionsTestSet().Count;
            
            Assert.Equal(expected, observed);
        }
    }
}
