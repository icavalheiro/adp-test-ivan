using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ADP.Test.BackEnd.Data;
using ADP.Test.BackEnd.Models;
using ADP.Test.BackEnd.Services;
using ADP.Test.BackEnd.Tests.Helpers;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.EntityFrameworkCore;
using Xunit;

namespace ADP.Test.BackEnd.Tests
{
    public class SessionServiceShould
    {
        private static DatabaseContext Database => GetDb();
        private static ILogger<SessionService> Logger => new Mock<ILogger<SessionService>>().Object;
        private static IQuestionService QuestionService => new QuestionService(
            Database, 
            new Mock<ILogger<QuestionService>>().Object);

        private static DatabaseContext GetDb()
        {
            var databaseMock = new Mock<DatabaseContext>();

            var questions = TestSetsHelper.GetQuestionsTestSet();

            databaseMock
                .Setup(db => db.Questions)
                .ReturnsDbSet(questions);

            databaseMock
                .Setup(db => db.Sessions)
                .ReturnsDbSet(new List<Session>());

            databaseMock
                .Setup(db => db.Answers)
                .ReturnsDbSet(new List<SessionQuestionAnswer>());
            
            return databaseMock.Object;
        }

        [Fact]
        public async Task GetNewSession()
        {
            var instance = new SessionService(Database, Logger, QuestionService);
            var newSession = await instance.GetNewSessionAsync();

            var observed = newSession.Id;
            var notExpected = Guid.Empty;
            
            Assert.NotEqual(notExpected, observed);
        }
        
        [Fact]
        public async Task GetNewSessions()
        {
            var instance = new SessionService(Database, Logger, QuestionService);
            var observed1 = await instance.GetNewSessionAsync();
            var observer2 = await instance.GetNewSessionAsync();

            Assert.NotEqual(observed1.Id, observer2.Id);
        }
        
        [Fact]
        public async Task SaveSessionToDatabase()
        {
            var databaseMock = new Mock<DatabaseContext>();

            var questions = TestSetsHelper.GetQuestionsTestSet();

            databaseMock
                .Setup(db => db.Questions)
                .ReturnsDbSet(questions);

            databaseMock
                .Setup(db => db.Sessions)
                .ReturnsDbSet(new List<Session>());

            databaseMock
                .Setup(db => db.Answers)
                .ReturnsDbSet(new List<SessionQuestionAnswer>());
            
            databaseMock.Setup(x => x.SaveChangesAsync(It.IsAny<CancellationToken>())).ReturnsAsync(1);
            
            var instance = new SessionService(databaseMock.Object, Logger, QuestionService);
            
            var newSession = await instance.GetNewSessionAsync();
            
            databaseMock.Verify(db => db.Sessions.Add(newSession));
            databaseMock.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()));
        }
        
        [Fact]
        public async Task ReturnUnansweredQuestions()
        {
            var sessionId = Guid.Parse("b05f169c-cb04-45c4-a1f9-b6a41ac46295");
            var databaseMock = new Mock<DatabaseContext>();

            var questions = TestSetsHelper.GetQuestionsTestSet();

            databaseMock
                .Setup(db => db.Questions)
                .ReturnsDbSet(questions);

            var sessions = new List<Session> {new Session {Id = sessionId}};
            databaseMock
                .Setup(db => db.Sessions)
                .ReturnsDbSet(sessions);

            databaseMock
                .Setup(db => db.Answers)
                .ReturnsDbSet(new List<SessionQuestionAnswer>());
            
            var instance = new SessionService(databaseMock.Object, Logger, QuestionService);
            var unanswered = await instance.GetUnansweredQuestionsAsync(sessionId);

            var observed = unanswered.Count();
            var expected = TestSetsHelper.GetQuestionsTestSet().Count;
            
            Assert.Equal(expected, observed);
        }
        
        [Fact]
        public async Task FiltersAnsweredQuestions()
        {
            var sessionId = Guid.Parse("b05f169c-cb04-45c4-a1f9-b6a41ac46295");
            var databaseMock = new Mock<DatabaseContext>();

            var questions = TestSetsHelper.GetQuestionsTestSet();
            var answeredQuestion = questions[0];

            databaseMock
                .Setup(db => db.Questions)
                .ReturnsDbSet(questions);

            var sessions = new List<Session> {new Session {Id = sessionId}};
            databaseMock
                .Setup(db => db.Sessions)
                .ReturnsDbSet(sessions);

            var answers = new List<SessionQuestionAnswer>
            {
                new SessionQuestionAnswer
                {
                    Id = Guid.NewGuid(),
                    Answer = 0,
                    Question = answeredQuestion,
                    QuestionId = answeredQuestion.Id,
                    Session = sessions[0],
                    SessionId = sessionId
                }
            };
            databaseMock
                .Setup(db => db.Answers)
                .ReturnsDbSet(answers);
            
            var instance = new SessionService(databaseMock.Object, Logger, QuestionService);
            var unanswered = await instance.GetUnansweredQuestionsAsync(sessionId);

            var observed = unanswered.Count();
            var expected = TestSetsHelper.GetQuestionsTestSet().Count - 1;
            
            Assert.Equal(expected, observed);
        }
        
        [Fact]
        public async Task ReturnNullAnsweredQuestionsForEmptySessionId()
        {
            var instance = new SessionService(Database, Logger, QuestionService);
            var observed = await instance.GetUnansweredQuestionsAsync(Guid.Empty);

            Assert.Null(observed);
        }
        
        [Fact]
        public async Task ReturnNullAnsweredQuestionsForInvalidSession()
        {
            var instance = new SessionService(Database, Logger, QuestionService);
            var observed = await instance.GetUnansweredQuestionsAsync(Guid.Parse("55555555-dddd-aaaa-bbbb-cccccccccccc"));

            Assert.Null(observed);
        }
        
        [Fact]
        public async Task SaveAnswerToDatabase()
        {
            var sessionId = Guid.Parse("b05f169c-cb04-45c4-a1f9-b6a41ac46295");
            var databaseMock = new Mock<DatabaseContext>();

            var questions = TestSetsHelper.GetQuestionsTestSet();

            databaseMock
                .Setup(db => db.Questions)
                .ReturnsDbSet(questions);

            var sessions = new List<Session> {new Session {Id = sessionId}};
            databaseMock
                .Setup(db => db.Sessions)
                .ReturnsDbSet(sessions);


            databaseMock
                .Setup(db => db.Answers)
                .ReturnsDbSet(new List<SessionQuestionAnswer>());
            
            databaseMock.Setup(x => x.SaveChangesAsync(It.IsAny<CancellationToken>())).ReturnsAsync(1);
            
            var instance = new SessionService(databaseMock.Object, Logger, QuestionService);
            
            await instance.AnswerQuestionAsync(sessionId, questions[0].Id, 0);
            
            databaseMock.Verify(db => db.Answers.Add(It.IsAny<SessionQuestionAnswer>()));
            databaseMock.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()));
        }
        
        [Fact]
        public async Task ReturnAnsweredQuestionsResults()
        {
            var sessionId = Guid.Parse("b05f169c-cb04-45c4-a1f9-b6a41ac46295");
            var databaseMock = new Mock<DatabaseContext>();

            var questions = TestSetsHelper.GetQuestionsTestSet();

            databaseMock
                .Setup(db => db.Questions)
                .ReturnsDbSet(questions);

            var sessions = new List<Session> {new Session {Id = sessionId}};
            databaseMock
                .Setup(db => db.Sessions)
                .ReturnsDbSet(sessions);

            var answers = new List<SessionQuestionAnswer>
            {
                new SessionQuestionAnswer
                {
                    Id = Guid.NewGuid(),
                    Answer = 0,
                    Question = questions[0],
                    QuestionId = questions[0].Id,
                    Session = sessions[0],
                    SessionId = sessions[0].Id
                }
            };
            
            databaseMock
                .Setup(db => db.Answers)
                .ReturnsDbSet(answers);
            
            var instance = new SessionService(databaseMock.Object, Logger, QuestionService);
            var answered = await instance.GetResultsAsync(sessionId);

            var observed = answered.Count();
            var expected = answers.Count;
            
            Assert.Equal(expected, observed);
        }
    }
}