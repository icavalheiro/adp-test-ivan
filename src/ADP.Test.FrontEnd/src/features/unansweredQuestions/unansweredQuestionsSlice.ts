import { createSlice } from "@reduxjs/toolkit";

function update ( state, action )
{
  let list: string[] = action.payload;
  state.value = list;

  if ( !!list && list.length > 0 )
    state.nextToAsk = list[ 0 ];
  else
    state.nextToAsk = null;
}

function remove ( state, action )
{
  update( state, {
    payload: state.value.filter( x => x != action.payload )
  } );
}

export const unansweredQuestionsSlice = createSlice( {
  name: "unansweredQuestions",
  initialState: {
    value: [],
    nextToAsk: null,
  },
  reducers: {
    updateUnansweredQuestions: update,
    removeUnansweredQuestion: remove,
  }
} );

export const { updateUnansweredQuestions, removeUnansweredQuestion } = unansweredQuestionsSlice.actions;
export default unansweredQuestionsSlice.reducer;