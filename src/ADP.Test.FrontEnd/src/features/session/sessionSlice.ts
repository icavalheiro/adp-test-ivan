import { createSlice } from "@reduxjs/toolkit";
import { cleanSessionId, getSessionId } from "../../services/sessionService";

export const sessionSlice = createSlice( {
  name: "session",
  initialState: {
    id: getSessionId(),
    isGameStarted: !!getSessionId(),
    isWaitingForNewSessionId: false,
  },
  reducers: {
    updateSession: ( state, action ) =>
    {
      state.isWaitingForNewSessionId = false;
      state.id = action.payload;
    },
    newSessionBeingLoaded: ( state ) =>
    {
      state.isWaitingForNewSessionId = true;
    },
    startGame: ( state ) =>
    {
      state.isGameStarted = true;
    },
    resetGame: ( state ) =>
    {
      state.id = cleanSessionId();
      state.isGameStarted = false;
    }
  }
} );

export const { updateSession, startGame, resetGame, newSessionBeingLoaded } = sessionSlice.actions;
export default sessionSlice.reducer;