export default interface QuestionResult {
  questionId:string;
  answer:number;
  isRight:boolean;
}