export default interface Question {
  id :string;
  category:string;
  difficulty:string;
  question:string;
  options:string[];
}