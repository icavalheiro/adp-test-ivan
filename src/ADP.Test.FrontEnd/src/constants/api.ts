export const apiUrl = "https://localhost:5001/api/";
export const newSessionApiUrl = apiUrl + 'session/new';
export const unansweredQuestionsApiUrl = apiUrl + 'session/unanswered';
export const questionsApiUrl = apiUrl + 'question/';
export const answerQuestionApiUrl = apiUrl + 'session/answer/';// +{questionId}
export const resultsApiUrl = apiUrl + 'session/results';
export const questionsCount = 10;