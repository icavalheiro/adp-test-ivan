import { configureStore } from "@reduxjs/toolkit";
import questionsReducer from "./features/questions/questionsSlice";
import unansweredQuestionsReducer from "./features/unansweredQuestions/unansweredQuestionsSlice";
import sessionReducer from "./features/session/sessionSlice";

export default configureStore( {
  reducer: {
    questions: questionsReducer,
    unansweredQuestions: unansweredQuestionsReducer,
    session: sessionReducer,
  }
} );