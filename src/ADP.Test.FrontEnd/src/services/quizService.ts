import { Dispatch } from "react";
import { updateQuestions } from "../features/questions/questionsSlice";
import { updateUnansweredQuestions } from "../features/unansweredQuestions/unansweredQuestionsSlice";
import { getQuestions } from "./questionService";
import { getUnansweredQuestions } from "./sessionService";

// Load the unanswered questions and the available questions into the store
export function loadQuizData ( sessionId: string, dispatch: Dispatch<any> )
{
  //load unanswered
  let unansweredPromise = getUnansweredQuestions( sessionId )
    .catch( err =>
    {
      alert( 'failed to load unanswered questions' );
      console.error( err );
    } );

  //load available questions
  let questionsPromise = getQuestions()
    .catch( err =>
    {
      alert( 'failed to load questions' );
      console.error( err );
    } );

  //wait for all to finish in any order
  return Promise.all( [ unansweredPromise, questionsPromise ] )
    .then( ( [ unansweredQuestionsResponse, questionsResponse ] ) =>
    {
      console.log( 'called updates for questions and unanswered questions' );
      dispatch( updateQuestions( questionsResponse ) );
      dispatch( updateUnansweredQuestions( unansweredQuestionsResponse ) );
    } )
    .catch( err =>
    {
      alert( 'something went wrong when loading data from api' );
      console.log( err );
    } );
}