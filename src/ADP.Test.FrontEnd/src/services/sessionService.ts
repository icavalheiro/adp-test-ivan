import axios from "axios";
import { sessionIdStorageKey } from "../constants/storageKeys";
import
{
  newSessionApiUrl,
  unansweredQuestionsApiUrl,
  answerQuestionApiUrl,
  resultsApiUrl
} from "../constants/api";
import QuestionResult from "../models/QuestionResult";

// Get the current session id, might be empty
export function getSessionId ()
{
  return sessionStorage.getItem( sessionIdStorageKey );
}

// Cleans the session id from the storage
export function cleanSessionId (): null
{
  sessionStorage.setItem( sessionIdStorageKey, "" );
  return null;
}

// Get a new session id from the API
export async function getNewSessionId ()
{
  try
  {
    let response = await axios.get( newSessionApiUrl );
    let { data } = response;
    sessionStorage.setItem( sessionIdStorageKey, data.id );
    return <string> data.id;
  }
  catch ( err )
  {
    alert( 'failed to fetch new session id from the server api' );
    console.error( err );
  }
}

// Returns a list of all the ids of the answers that have been
// already answered
export async function getUnansweredQuestions ( sessionId: string )
{
  try
  {
    let response = await axios.get( unansweredQuestionsApiUrl + '?sessionId=' + sessionId );
    let { data } = response;
    return <string[]> data;
  }
  catch ( err )
  {
    alert( 'failed to fetch unanswered questions from the server' );
    console.error( err );
  }
}

// Sends a post request to the API that saves the given answer
// into the database
export async function postAnswer ( sessionId: string, questionId: string, answerIndex: number )
{
  try
  {
    let payload = { answer: answerIndex };

    await axios.post(
      answerQuestionApiUrl + questionId + '?sessionId=' + sessionId,
      payload );
  }
  catch ( err )
  {
    alert( 'failed to post answer to the server' );
    console.error( err );
  }
}

// Get the results of the answered questions for the given
// session id
export async function getResults ( sessionId: string )
{
  try
  {
    let response = await axios.get( resultsApiUrl + '?sessionId=' + sessionId );
    let { data } = response;
    return <QuestionResult[]> data;
  }
  catch ( err )
  {
    alert( 'failed to retrieve results from the server' );
    console.error( err );
  }
}


