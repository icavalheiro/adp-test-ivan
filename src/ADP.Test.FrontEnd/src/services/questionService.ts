import axios from "axios";
import { questionsApiUrl } from "../constants/api";
import Question from "../models/Question";

// Return a list of all available questions in the API
export async function getQuestions ()
{
  try
  {
    let response = await axios.get( questionsApiUrl );
    let { data } = response;
    return <Question[]> data;
  }
  catch ( err )
  {
    alert( 'failed to fetch questions from the server api' );
    console.error( err );
  }
}
