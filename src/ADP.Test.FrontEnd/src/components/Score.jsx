import { useEffect, useState } from "react";
import { getResults } from "../services/sessionService";
import { useDispatch, useSelector } from "react-redux";
import { resetGame } from "../features/session/sessionSlice";
import Button from "./Button";

function Score () {
  const dispatch = useDispatch();

  const sessionId = useSelector( ( store ) => store.session.id );
  const questions = useSelector( ( store ) => store.questions.value );

  let [ results, setResults ] = useState( null );

  useEffect( () => {
    getResults( sessionId )
      .then( setResults );
  }, [ sessionId ] );

  function getRightResultsCount () {
    let rightResults = results.filter( x => x.isRight );
    return rightResults.length;
  }

  function getResultQuestion ( result ) {
    return questions.find( x => x.id == result.questionId );
  }

  function getAnswerLabel ( result ) {
    let answerIndex = result.answer;
    let question = getResultQuestion( result );
    return question.options[ answerIndex ];
  }

  function onPlayAgainClicked () {
    dispatch( resetGame() );
  }

  return (
    <div className="score-component">
      {
        ( !results )
          ? <p>Loading results...</p>
          : <>
            <h1>
              You scored<br />
              { getRightResultsCount() } / { results.length }
            </h1>
            <div className="card">
              {
                results.map( ( result, i ) => (
                  <div
                    className={ "question-result " + ( result.isRight ? "true" : "false" ) }
                    key={ i }
                  >
                    { result.isRight ? "✅" : "❌" } { getResultQuestion( result ).question }

                    {
                      ( !result.isRight )
                        ? <span className="correct-answer-explanation">
                          You said it was { getAnswerLabel( result ) }
                        </span>
                        : ""
                    }
                  </div>
                ) )
              }
            </div>
            <div className="playa-again-wrapper">
              <Button onClick={ onPlayAgainClicked }>PLAY AGAIN?</Button>
            </div>
          </>
      }
    </div>
  );

}

export default Score;
