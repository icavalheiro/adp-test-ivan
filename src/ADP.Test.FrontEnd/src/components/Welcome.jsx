import { questionsCount } from '../constants/api';
import { useDispatch } from "react-redux";
import { getNewSessionId } from "../services/sessionService";
import { newSessionBeingLoaded, startGame, updateSession } from "../features/session/sessionSlice";
import { useSelector } from 'react-redux';
import Button from "./Button";

function Welcome () {
  const dispatch = useDispatch();
  const isWaitingForNewSessionId = useSelector( ( store ) => store.session.isWaitingForNewSessionId );
  function dispatchStartNewGame () {
    dispatch( newSessionBeingLoaded() );

    getNewSessionId()
      .then( newSessionId => {
        dispatch( updateSession( newSessionId ) );
        dispatch( startGame() );
      } );
  }

  return (
    <div className="welcome-component">
      <h1>Welcome to the Trivia Challange!</h1>
      <div className="card">
        <p>You will be presented with { questionsCount } True or False questions.</p>
        <p>Can you score 100%?</p>
      </div>
      <p>
        {
          !isWaitingForNewSessionId
            ? <Button onClick={ dispatchStartNewGame }>BEGIN</Button>
            : <Button disabled={ true }>LOADING...</Button>
        }
      </p>
    </div>
  );
}

export default Welcome;
