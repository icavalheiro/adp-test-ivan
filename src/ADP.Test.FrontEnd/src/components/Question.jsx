import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { questionsCount } from "../constants/api";
import { postAnswer } from "../services/sessionService";
import { removeUnansweredQuestion } from "../features/unansweredQuestions/unansweredQuestionsSlice";
import Button from "./Button";

function Question () {
  let [ isPostingAnswer, setIsPostingAnswer ] = useState( false );
  const dispatch = useDispatch();
  const sessionId = useSelector( ( store ) => store.session.id );

  const answredQuestions = useSelector(
    ( store ) => questionsCount - store.unansweredQuestions.value.length
  );

  const question = useSelector(
    ( store ) => store.questions.value
      .find( x => x.id == store.unansweredQuestions.nextToAsk )
  );


  function onAnswerSelected ( answerIndex ) {
    setIsPostingAnswer( true );
    postAnswer( sessionId, question.id, answerIndex )
      .then( () => {
        setIsPostingAnswer( false );
        dispatch( removeUnansweredQuestion( question.id ) );
      } )
      .catch( err => {
        alert( 'failed to post answer to the api' );
        console.error( err );
      } );
  }

  return (
    <div className="question-component">
      <h2>{ question.category }</h2>
      <div className="card">
        <p className="question-box">
          { question.question }
        </p>
        <p className="question-counter">
          <span>
            { ( answredQuestions + 1 ) } of { questionsCount }
          </span>
        </p>
      </div>
      <div className="options-wrapper">
        {
          question.options.map( ( option, i ) =>
            <Button disabled={ isPostingAnswer } key={ i } onClick={ () => onAnswerSelected( i ) } tint={ option }>
              { option }
            </Button>
          )
        }
      </div>
    </div>
  );

}

export default Question;
