import Welcome from "./Welcome";
import Quiz from "./Quiz";
import { useSelector } from "react-redux";


function App () {
  const sessionId = useSelector( ( store ) => store.session.id );
  const isGameStarted = useSelector( ( store ) => store.session.isGameStarted );
  const isWaitingForNewSessionId = useSelector( ( store ) => store.session.isWaitingForNewSessionId );

  return (
    //#root 
    <div className="app-wrapper">
      <div className="app-box">
        {
          ( !isGameStarted || isWaitingForNewSessionId )
            ? <Welcome />
            : <Quiz />
        }
      </div>
      <p className="session-id">
        Current session id: <br />
        { ( !!sessionId ) ? sessionId : "session not started yet" }
      </p>
    </div>
  );
}

export default App;
