function Button ( props ) {
  let onClick = props.onClick;
  let disabled = props.disabled;
  let children = props.children;
  let leftText = props.leftText;
  let tint = props.tint || "";
  return (
    <button onClick={ onClick }
      disabled={ disabled }
      className={ "button-component " + ( !leftText ? "center " : "left " ) + tint.toLowerCase() }>
      { children }
    </button>
  );
}

export default Button;