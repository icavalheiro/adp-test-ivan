import { useState, useEffect } from "react";
import Question from "./Question";
import Score from "./Score";
import { useDispatch, useSelector } from "react-redux";
import { loadQuizData } from "../services/quizService";

function Quiz () {
  const sessionId = useSelector( ( store ) => store.session.id );
  const unansweredQuestions = useSelector( ( store ) => store.unansweredQuestions.value );
  const dispatch = useDispatch();

  let [ loading, setLoading ] = useState( true );

  useEffect( () => {
    loadQuizData( sessionId, dispatch )
      .then( () => setLoading( false ) );
  }, [ sessionId ] );

  function shouldShowScore () {
    return unansweredQuestions.length == 0;
  }

  return (
    <div className="quiz-component">
      {
        ( loading )
          ? <p>Loading...</p>
          : ( shouldShowScore() )
            ? <Score />
            : <Question />
      }

    </div>
  );
}

export default Quiz;
