using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;

namespace ADP.Test.BackEnd.Models
{
    public class Question
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("category")] 
        [Required]
        public string Category { get; set; }
        
        [JsonPropertyName("difficulty")] 
        [Required]
        public string Difficulty { get; set; }

        [JsonPropertyName("question")]
        [Required]
        public string Text { get; set; }

        [NotMapped]
        [JsonPropertyName("options")]
        public string[] Options
        {
            get => SerializedOptions.Split("{|}"); 
            set => SerializedOptions = string.Join("{|}", value);
        }
        
        [JsonIgnore]
        public string SerializedOptions { get; set; }
        
        [JsonIgnore]
        public int RightOption { get; set; }
    }
}