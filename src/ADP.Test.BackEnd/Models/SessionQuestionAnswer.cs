using System;

namespace ADP.Test.BackEnd.Models
{
    public class SessionQuestionAnswer
    {
        public Guid Id { get; set; }
        
        public Guid SessionId { get; set; }
        public Session Session { get; set; }
        
        public Guid QuestionId { get; set; }
        public Question Question { get; set; }
        public int Answer { get; set; }
        
        public bool IsRight => this.Question.RightOption == Answer;
    }
}