namespace ADP.Test.BackEnd.Models
{
    public class AnswerQuestionPostBody
    {
        public int Answer { get; set; }
    }
}