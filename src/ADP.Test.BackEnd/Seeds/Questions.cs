using System;
using ADP.Test.BackEnd.Models;
using Microsoft.EntityFrameworkCore;

namespace ADP.Test.BackEnd.Seeds
{
    public static class Questions
    {
        /// <summary>
        /// Loads 10 questions into the database
        /// </summary>
        public static void SeedQuestions(this ModelBuilder modelBuilder)
        {
            var random = new Random(12554455);
            var guid = new byte[16];
            Guid GetGuid()
            {
                random.NextBytes(guid);
                return new Guid(guid);
            };
            
            modelBuilder.Entity<Question>().HasData(new Question[]
            {
                new Question
                {
                    Id = GetGuid(),
                    Category = "History",
                    Difficulty = "hard",
                    Text = "Japan was part of the Allied Powers during World War I.",
                    Options = new string []
                    {
                        "True",
                        "False"
                    },
                    RightOption = 0
                },
                new Question
                {
                    Id = GetGuid(),
                    Category = "History",
                    Difficulty = "hard",
                    Text = "Joseph Stalin's real name was Ioseb Bessarionis dze Dzugashvili.",
                    Options = new string []
                    {
                        "True",
                        "False"
                    },
                    RightOption = 0
                },
                new Question
                {
                    Id = GetGuid(),
                    Category = "Entertainment: Music",
                    Difficulty = "hard",
                    Text = "The song \"Mystery Train\" was released by artist \"Little Junior's Blue Flames\" in 1953.",
                    Options = new string []
                    {
                        "True",
                        "False"
                    },
                    RightOption = 0
                },
                new Question
                {
                    Id = GetGuid(),
                    Category = "Entertainment: Music",
                    Difficulty = "hard",
                    Text = "Pete Townshend's solo album, \"White City: A Novel\", is set in the metropolitan area of Chicago.",
                    Options = new string []
                    {
                        "True",
                        "False"
                    },
                    RightOption = 1
                },
                new Question
                {
                    Id = GetGuid(),
                    Category = "Entertainment: Japanese Anime & Manga",
                    Difficulty = "hard",
                    Text = "In the \"Kagerou Daze\" series, Shintaro Kisaragi is prominently shown with the color red.",
                    Options = new string []
                    {
                        "True",
                        "False"
                    },
                    RightOption = 0
                },
                new Question
                {
                    Id = GetGuid(),
                    Category = "Mythology",
                    Difficulty = "hard",
                    Text = "Janus was the Roman god of doorways and passageways.",
                    Options = new string []
                    {
                        "True",
                        "False"
                    },
                    RightOption = 0
                },
                new Question
                {
                    Id = GetGuid(),
                    Category = "Entertainment: Japanese Anime & Manga",
                    Difficulty = "hard",
                    Text = "The character Plum from \"No Game No Life\" is a girl.",
                    Options = new string []
                    {
                        "True",
                        "False"
                    },
                    RightOption = 1
                },
                new Question
                {
                    Id = GetGuid(),
                    Category = "Vehicles",
                    Difficulty = "hard",
                    Text = "The term \"GTO\" was originated by Ferrari?",
                    Options = new string []
                    {
                        "True",
                        "False"
                    },
                    RightOption = 0
                },
                new Question
                {
                    Id = GetGuid(),
                    Category = "Entertainment: Video Games",
                    Difficulty = "hard",
                    Text = "In \"Minecraft\", gold tools are faster than diamond tools.",
                    Options = new string []
                    {
                        "True",
                        "False"
                    },
                    RightOption = 0
                },
                new Question
                {
                    Id = GetGuid(),
                    Category = "Entertainment: Film",
                    Difficulty = "hard",
                    Text = "YouTube personality Jenna Marbles served as an executive producer of the film Maximum Ride (2016).",
                    Options = new string []
                    {
                        "True",
                        "False"
                    },
                    RightOption = 0
                },
            });
        }
    }
}