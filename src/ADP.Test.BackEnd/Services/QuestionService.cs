using System;
using System.Linq;
using System.Threading.Tasks;
using ADP.Test.BackEnd.Data;
using ADP.Test.BackEnd.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace ADP.Test.BackEnd.Services
{
    public interface IQuestionService
    {
        Task<Question> GetQuestionAsync(Guid id);
        Task<IQueryable<Question>> GetAllQuestionsAsync();
    }
    
    public class QuestionService : IQuestionService
    {
        private readonly DatabaseContext _database;
        private readonly ILogger<QuestionService> _logger;

        public QuestionService(DatabaseContext database, ILogger<QuestionService> logger)
        {
            _database = database;
            _logger = logger;
        }

        /// <summary>
        /// Returns the question with the given ID.
        /// </summary>
        /// <param name="id">Valid question ID</param>
        /// <returns>The question or null in case of a invalid question ID</returns>
        public async Task<Question> GetQuestionAsync(Guid id)
        {
            var query = _database.Questions.Where(x => x.Id == id);
            var result = await query.FirstOrDefaultAsync();
            if (result == null)
            {
                _logger.Log(LogLevel.Warning,"Trying to get Question {Id}, but nothing found", id);
            }

            _logger.Log(LogLevel.Information, "Fetched question {Id} from database", id);
            return result;
        }

        /// <summary>
        /// Retrieves a query with all the questions available in the database.
        /// </summary>
        /// <returns>Query with all the questions available</returns>
        public async Task<IQueryable<Question>> GetAllQuestionsAsync()
        {
            var query = _database.Questions;

            var resultCount = await query.CountAsync();
            if (resultCount == 0)
            {
                _logger.Log(LogLevel.Error,"Trying to get all Questions, but there's none in database");                
            }
            
            _logger.Log(LogLevel.Information, "Queried all questions from the database");

            return query;
        }
    }
}