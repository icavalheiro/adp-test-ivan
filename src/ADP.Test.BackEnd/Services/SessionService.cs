using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ADP.Test.BackEnd.Data;
using ADP.Test.BackEnd.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace ADP.Test.BackEnd.Services
{
    public interface ISessionService
    {
        Task<Session> GetNewSessionAsync();
        Task<IEnumerable<Guid>> GetUnansweredQuestionsAsync(Guid session);
        Task AnswerQuestionAsync(Guid session, Guid questionId, int answer);
        Task<IEnumerable<(Guid questionId, int answer, bool isRight)>> GetResultsAsync(Guid session);
    }
    
    public class SessionService : ISessionService
    {
        private readonly DatabaseContext _database;
        private readonly ILogger<SessionService> _logger;
        private readonly IQuestionService _questionService;

        public SessionService(DatabaseContext database, ILogger<SessionService> logger, IQuestionService questionService)
        {
            _database = database;
            _logger = logger;
            _questionService = questionService;
        }

        /// <summary>
        /// Get a new valid session object (also saves it into the database)
        /// </summary>
        public async Task<Session> GetNewSessionAsync()
        {
            var newSession = new Session { Id = Guid.NewGuid() };
            _database.Sessions.Add(newSession);
            await _database.SaveChangesAsync();
            _logger.Log(LogLevel.Information, "New Sessions created {Id}", newSession.Id);
            return newSession;
        }

        private async Task<bool> CheckValidSession(Guid session)
        {
            if (session == Guid.Empty)
                return false;

            var isValidSession = await _database.Sessions.Where(x => x.Id == session).CountAsync() == 1;

            return isValidSession;
        }

        /// <summary>
        /// Get the IDs of all the questions not answered in the given session.
        /// </summary>
        /// <param name="session">Valid Session ID</param>
        public async Task<IEnumerable<Guid>> GetUnansweredQuestionsAsync(Guid session)
        {
            if (!await CheckValidSession(session))
            {
                _logger.Log(LogLevel.Warning, "Someone tried to load unanswered questions, but the session id is invalid: {Id}", session);
                return null;
            }
            
            var questions = await _questionService.GetAllQuestionsAsync();

            var answersQuery = _database.Answers
                .Where(x => x.Session.Id == session);
            
            var answeredIds = answersQuery.Select(x => x.QuestionId);

            var unansweredQuery = questions.Where(x => !answeredIds.Contains(x.Id));
            var unansweredIds = unansweredQuery.Select(x => x.Id);
            
            _logger.Log(LogLevel.Information, "Unanswered questions fetched for session {Id}", session);

            return await unansweredIds.ToArrayAsync();
        }

        /// <summary>
        /// Saves into the database the given answer to the given question for the given session.
        /// </summary>
        /// <param name="sessionId">Valid Session ID</param>
        /// <param name="questionId">ID of the question being answered</param>
        /// <param name="answer">The INDEX of the chosen answer</param>
        public async Task AnswerQuestionAsync(Guid sessionId, Guid questionId, int answer)
        {
            var session = await _database.Sessions.Where(x => x.Id == sessionId).FirstOrDefaultAsync();
            if (session == null)
            {
                _logger.Log(LogLevel.Warning, "Tried to answer a question, but session id is invalid {Id}", sessionId);
                return;
            }

            var question = await _database.Questions.Where(x => x.Id == questionId).FirstOrDefaultAsync();
            if (question == null)
            {
                _logger.Log(LogLevel.Warning, "Session {SessionId} tried to answer a question, but question id is invalid {Id}", sessionId,questionId);
                return;
            }

            var existingAnswer = await _database.Answers
                .Where(x => x.QuestionId == questionId && x.SessionId == sessionId)
                .FirstOrDefaultAsync();

            if (existingAnswer != null)
            {
                existingAnswer.Answer = answer;
                _database.Answers.Update(existingAnswer);
            }
            else
            {
                var questionAnswer = new SessionQuestionAnswer
                {
                    Id = Guid.NewGuid(),
                    Answer = answer,
                    Question = question,
                    Session = session
                };

                _database.Answers.Add(questionAnswer);
            }

            await _database.SaveChangesAsync();
            _logger.Log(LogLevel.Information, "Question {QuestionId} answered by session {SessionId} and saved into db", questionId, sessionId);
        }

        /// <summary>
        /// For the given Session returns all answered questions and if their results are right or not.
        /// </summary>
        /// <param name="sessionId">Valid Session ID</param>
        /// <returns>A tuple containing the ID of the question, the index of the chosen answer
        /// and whether if it's right or not</returns>
        public async Task<IEnumerable<(Guid questionId, int answer, bool isRight)>> GetResultsAsync(Guid sessionId)
        {
            var session = await _database.Sessions.Where(x => x.Id == sessionId).FirstOrDefaultAsync();
            if (session == null)
            {
                _logger.Log(LogLevel.Warning, "Tried to query answered questions, but session id is invalid {Id}", sessionId);
                return null;
            }

            var answersQuery = _database.Answers.Where(x => x.SessionId == sessionId)
                .Include(x => x.Question)
                .Include(x => x.Session);
            var answers = answersQuery.AsEnumerable().Select(x => (x.Question.Id, x.Answer, x.IsRight)); 
            _logger.Log(LogLevel.Information, "Retrieved answered questions by session {Id}", sessionId);
            return answers.ToArray();
        }
    }
}