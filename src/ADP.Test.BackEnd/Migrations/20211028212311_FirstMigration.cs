﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ADP.Test.BackEnd.Migrations
{
    public partial class FirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Questions",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Category = table.Column<string>(type: "TEXT", nullable: false),
                    Difficulty = table.Column<string>(type: "TEXT", nullable: false),
                    Text = table.Column<string>(type: "TEXT", nullable: false),
                    SerializedOptions = table.Column<string>(type: "TEXT", nullable: true),
                    RightOption = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sessions",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sessions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Answers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    SessionId = table.Column<Guid>(type: "TEXT", nullable: true),
                    QuestionId = table.Column<Guid>(type: "TEXT", nullable: true),
                    Answer = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Answers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Answers_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "Questions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Answers_Sessions_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Sessions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "Category", "Difficulty", "RightOption", "SerializedOptions", "Text" },
                values: new object[] { new Guid("d0255064-db3f-5b21-da9c-c6c9b8dfe273"), "History", "hard", 0, "True{|}False", "Japan was part of the Allied Powers during World War I." });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "Category", "Difficulty", "RightOption", "SerializedOptions", "Text" },
                values: new object[] { new Guid("3486f631-cb11-1ca3-3aad-bd679f16e933"), "History", "hard", 0, "True{|}False", "Joseph Stalin's real name was Ioseb Bessarionis dze Dzugashvili." });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "Category", "Difficulty", "RightOption", "SerializedOptions", "Text" },
                values: new object[] { new Guid("d8d4cba7-f39c-5850-2791-19dcc7b1007b"), "Entertainment: Music", "hard", 0, "True{|}False", "The song \"Mystery Train\" was released by artist \"Little Junior's Blue Flames\" in 1953." });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "Category", "Difficulty", "RightOption", "SerializedOptions", "Text" },
                values: new object[] { new Guid("9ccd873d-516f-9999-ac08-96911db9bcc3"), "Entertainment: Music", "hard", 1, "True{|}False", "Pete Townshend's solo album, \"White City: A Novel\", is set in the metropolitan area of Chicago." });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "Category", "Difficulty", "RightOption", "SerializedOptions", "Text" },
                values: new object[] { new Guid("ed2293b2-090a-3ed7-a52e-0c7fb2c75488"), "Entertainment: Japanese Anime & Manga", "hard", 0, "True{|}False", "In the \"Kagerou Daze\" series, Shintaro Kisaragi is prominently shown with the color red." });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "Category", "Difficulty", "RightOption", "SerializedOptions", "Text" },
                values: new object[] { new Guid("172a42ac-4d49-55c4-313b-2c935dbe3a6e"), "Mythology", "hard", 0, "True{|}False", "Janus was the Roman god of doorways and passageways." });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "Category", "Difficulty", "RightOption", "SerializedOptions", "Text" },
                values: new object[] { new Guid("342955d5-138e-3470-b08e-f640441ae7e5"), "Entertainment: Japanese Anime & Manga", "hard", 1, "True{|}False", "The character Plum from \"No Game No Life\" is a girl." });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "Category", "Difficulty", "RightOption", "SerializedOptions", "Text" },
                values: new object[] { new Guid("dbe50db3-a48f-657a-cecc-bbcfdd44e0e7"), "Vehicles", "hard", 0, "True{|}False", "The term \"GTO\" was originated by Ferrari?" });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "Category", "Difficulty", "RightOption", "SerializedOptions", "Text" },
                values: new object[] { new Guid("5daa9ef4-209d-99f9-d1f6-67bb56831017"), "Entertainment: Video Games", "hard", 0, "True{|}False", "In \"Minecraft\", gold tools are faster than diamond tools." });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "Category", "Difficulty", "RightOption", "SerializedOptions", "Text" },
                values: new object[] { new Guid("4fdf4654-5ed9-30de-dbc4-65c258a0566c"), "Entertainment: Film", "hard", 0, "True{|}False", "YouTube personality Jenna Marbles served as an executive producer of the film Maximum Ride (2016)." });

            migrationBuilder.CreateIndex(
                name: "IX_Answers_QuestionId",
                table: "Answers",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_Answers_SessionId",
                table: "Answers",
                column: "SessionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Answers");

            migrationBuilder.DropTable(
                name: "Questions");

            migrationBuilder.DropTable(
                name: "Sessions");
        }
    }
}
