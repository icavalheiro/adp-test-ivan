using ADP.Test.BackEnd.Models;
using ADP.Test.BackEnd.Seeds;
using Microsoft.EntityFrameworkCore;

namespace ADP.Test.BackEnd.Data
{
    public class DatabaseContext : DbContext
    {
        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<Session> Sessions { get; set; }
        public virtual DbSet<SessionQuestionAnswer> Answers { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }
        
        public DatabaseContext(){}
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.SeedQuestions();
        }
    }
}