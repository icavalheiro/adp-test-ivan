using System;
using System.Globalization;
using System.Threading.Tasks;
using ADP.Test.BackEnd.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ADP.Test.BackEnd.Controllers
{
    [Route("/api/question/")]
    public class QuestionController : Controller
    {
        private readonly IQuestionService _questionService;
        private readonly ILogger<QuestionController> _logger;

        public QuestionController(IQuestionService questionService, ILogger<QuestionController> logger)
        {
            _questionService = questionService;
            _logger = logger;
        }

        [HttpGet("")]
        public async Task<IActionResult> Index()
        {
            var questions = await _questionService.GetAllQuestionsAsync();
            _logger.Log(LogLevel.Information, "Serving all questions");
            return Json(questions);
        }

        [HttpGet("{questionId:guid}")]
        public async Task<IActionResult> Find(Guid questionId)
        {
            var question = await _questionService.GetQuestionAsync(questionId);
            if (question == null)
            {
                _logger.Log(LogLevel.Warning, "Can't serve unfounded question {Id}", questionId);
                return NotFound();
            }
            
            _logger.Log(LogLevel.Information, "Serving question {Id}", question.Id);
            return Json(question);
        }
    }
}