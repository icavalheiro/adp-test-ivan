using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using ADP.Test.BackEnd.Models;
using ADP.Test.BackEnd.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ADP.Test.BackEnd.Controllers
{
    [Route("/api/session/")]
    public class SessionController : Controller
    {
        private readonly ISessionService _sessionService;
        private readonly ILogger<SessionController> _logger;

        public SessionController(ISessionService sessionService, ILogger<SessionController> logger)
        {
            _sessionService = sessionService;
            _logger = logger;
        }

        [HttpGet("new")]
        public async Task<IActionResult> New()
        {
            var newSession = await _sessionService.GetNewSessionAsync();
            _logger.Log(LogLevel.Information, "Serving new session {Id}", newSession.Id);
            return Json(newSession);
        }

        [HttpGet("unanswered")]
        public async Task<IActionResult> UnansweredQuestions([FromQuery]Guid sessionId)
        {
            var questions = await _sessionService.GetUnansweredQuestionsAsync(sessionId);
            if (questions == null)
            {
                _logger.Log(LogLevel.Warning, "Can't serve unanswered questions for session {Id}, something is invalid", sessionId);
                return NotFound();
            }

            _logger.Log(LogLevel.Information, "Serving unanswered questions for sessions {Id}", sessionId);
            return Json(questions);
        }

        [HttpPost("answer/{questionId:guid}")]
        public async Task<IActionResult> AnswerQuestion(Guid questionId, [FromQuery] Guid sessionId, [FromBody] AnswerQuestionPostBody body)
        {
            _logger.Log(LogLevel.Information, "Post received, answer to question {QuestionId} for session {SessionId}", questionId, sessionId);
            await _sessionService.AnswerQuestionAsync(sessionId, questionId, body.Answer);
            return Ok();
        }

        [HttpGet("results")]
        public async Task<IActionResult> Results([FromQuery] Guid sessionId)
        {
            var results = await _sessionService.GetResultsAsync(sessionId);
            if (results == null)
            {
                _logger.Log(LogLevel.Warning, "Can't serve results for session {Id}, something is invalid", sessionId);
                return NotFound();
            }
            
            _logger.Log(LogLevel.Information, "Serving results for session {Id}", sessionId);

            return Json(results.Select(x => new
            {
                questionId= x.questionId,
                answer=x.answer,
                isRight= x.isRight
            }));
        }
        
        
    }
}