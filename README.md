# ADP Test

Test project for ADP process.

This project aims to be a simple Quiz app, with a backend in .net and a react front-end

## Running the project

#### Back-end

Make sure you have installed:

- dotnet 5 >=


To run, just cd into `src/ADP.Test.BackEnd` and:

```bash
dotnet run
```

Documentation about the api can be found in `https://localhost:5001/swagger`


#### Front-end

Make sure you have installed:

- nodejs 12 >=


To run, just cd into `src/ADP.Test.FronEnd` and:

```bash
npm i
npm run dev
```

